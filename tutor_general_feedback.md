# General Feedback.
***
## Tests.
* Name test cases descriptively to say what they're testing - its okay if tests names are long
* Remember to put all your test cases in a folder.

## Comments
* Sufficient commenting includes both function and their definitions - commenting on the overall purpose of a function before its definition, but also explaining its individual parts in the function itself.

## Indentation and Spacing
* C uses snake case, no camel case - but if you do choose camel case, don't mix them together
in a code base (but choose snake case). Use lines between logical 'blocks' in each function.

## Functions and Modularity.
* Leave function declarations in the header file - if you've included the header file with the declarations, you don't need to copy them again into the .c file
* While not a blanket rule - whenever there is a loop, it can arguably be separated out into its own function, as long as the loop can be 'described' as performing a particular activity. Of course, don't overdo this and have 500 functions in a project that's only a few thousand lines long - use your own judgment.
* Separate out linked list functionality - don't include them in line every time you need them, as it's code repetition which means for the same functionality, one place may have a bug while another doesn't.

## Code Quality and Clarity
* C uses 4 space indentation levels, not 2.    
