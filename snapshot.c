/**
 * comp2129 - assignment 2
 * Cameron Nichols
 * cnic3139
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <stdbool.h>
#include <ctype.h>
#include <limits.h>

#include "snapshot.h"

// Global pointers to linked lists of entries and snapshots.
entry *entries;
snapshot *snapshots;

// ID number generator for snapshots.
int id = 1;

// List functions.
void list(char *command) {
	// Three possible options: keys, entries, snapshots.
	// run input through a lowercase function, as command is case insensitive.
	for (int i = 0; i < strlen(command); i++) {
		command[i] = tolower(command[i]);
	}

	if (!strcmp(command, "keys")) {
		// Iterate through entries, to find node.
		if (entries == NULL) {
			printf("no %s\n", command);
			return;
		}
		entry *current = entries;
		while (1) {
			printf("%s\n", current->key);
			if (current->next == NULL) {
				break;
			}
			current = current->next;
		}

	} else if (!strcmp(command, "entries")) {
		// Iterate through entries.
		if (entries == NULL) {
			printf("no %s\n", command);
			return;
		}

		entry *current = entries;
		while (1) {
			printf("%s ", current->key); // Print key of entry
			// Print list of values.
			printf("[");
			if (current->value_length >= 1) {
				 printf("%d", current->values[0]);
			}
			for (int i = 1; i < current->value_length; i++) {
				printf(" %d", current->values[i]);
			}
			printf("]\n");

			if (current->next == NULL) {
				break;
			}
			current = current->next;
		}

	} else if (!strcmp(command, "snapshots")){
		// Iterate through snapshots.

		if (snapshots == NULL) {
			printf("no %s\n", command);
			return;
		}

		snapshot *current = snapshots;
		while (1) {
			printf("%d\n", current->id);

			if (current->next == NULL) {
				break;
			}
			current = current->next;

		}

	}

}

// Entry functions.
void get(char *command) {
	// Iterate through entries, to find node.
	entry *current = entries;
	if (entries == NULL) {
		printf("no such key\n");
		return;
	}
	while (1) {
		if (!strcmp(command, current->key)) {
			printf("[");
			if (current->value_length >= 1) {
				 printf("%d", current->values[0]);
			}
			for (int i = 1; i < current->value_length; i++) {
				printf(" %d", current->values[i]);
			}
			printf("]\n");
			break;
		} else {
			if (current->next == NULL) {
				printf("no such key\n");
				break;
			}
			current = current->next;
		}
	}
}

int del(char *command) {
	int return_val = INT_MIN;
	// Iterate through entries, to find node.
	entry *current = entries;
	while (1) {
		if (current == NULL || entries == NULL) {
			printf("no such key\n");
			break;
		}

		if (!strcmp(command, current->key)) {

			// Freeing values list.
			free(current->values);

			// Freeing entry struct.
			if (current->prev != NULL) {
				current->prev->next = current->next;
			} else {
				entries = current->next;
			}
			if (current->next != NULL) {
				current->next->prev = current->prev;
			}
			free(current);
			return_val = 0;
			break;
		} else {
			if (current == NULL || current->next == NULL) {
				printf("no such key\n");
				break;
			}
			current = current->next;
		}
	}
	return return_val;
}

void purge(char *command) {
	// Delete from current state.
	entry *current = entries;
	while (1) {
		// If not in current state, skip to removal from snapshots.
		if (current == NULL || entries == NULL) {
			break;
		}

		if (!strcmp(command, current->key)) {
			// Freeing values list.
			free(current->values);

			// Freeing entry struct.
			if (current->prev != NULL) {
				current->prev->next = current->next;
			} else {
				if (current->next == NULL) {
					entries = NULL;
				} else {
					entries = current->next;
				}
			}
			if (current->next != NULL) {
				current->next->prev = current->prev;
			}
			free(current);
			break;
		} else {
			current = current->next;
			if (current == NULL) {
				break;
			}
		}
	}
	// Delete from snapshots.
	snapshot *cur_snap = snapshots;
	while (cur_snap != NULL) {
		// Iterate through for each snapshot.
		entry *cur_ent = cur_snap->entries;
		while (1) {

			if (cur_ent == NULL || cur_snap->entries == NULL) {
				break;
			}

			if (!strcmp(command, cur_ent->key)) {
				// Freeing values list.
				free(cur_ent->values);

				// Freeing entry struct.
				if (cur_ent->prev != NULL) {
					cur_ent->prev->next = cur_ent->next;
				} else {
					if (cur_ent->next == NULL) {
						cur_snap->entries = NULL;
					} else {
						cur_snap->entries = cur_ent->next;
					}
				}
				if (cur_ent->next != NULL) {
					cur_ent->next->prev = cur_ent->prev;
				}
				free(cur_ent);
				break;
			} else {
				cur_ent = cur_ent->next;
				if (cur_ent == NULL) {
					break;
				}
			}

		}

		cur_snap = cur_snap->next;
		if (cur_snap == NULL) {
			break;
		}

	}
	printf("ok\n");

}

void add_values(char *values, entry *node) {
	// Values.
	values = strtok(NULL, "\n "); // A value.
	while (values != NULL) {
		// If array is full, resize. Else, add value to array of values.
		if ((node->total_length)-1 == node->value_length) {
			node->values = realloc(node->values, (node->total_length * 2)*sizeof(int));
			node->total_length = (node->total_length * 2);
		} else {
			node->values[node->value_length] = atoi(values);
			node->value_length++;
			values = strtok(NULL, "\n "); // A value.
		}

	}
	printf("ok\n"); // Everything has succeeded.
}

// Value functions.
void set(char *command) {

	// If head of list is null, fill in empty struct with given values.
	if (entries == NULL) {

		// No node found matching key.
		entry *new_entry = calloc(1, sizeof(entry));
		new_entry->total_length = 4;
		new_entry->value_length = 0;
		new_entry->next = NULL;
		new_entry->prev = NULL;
		entries = new_entry;

		// Key.
		strcpy(&new_entry->key[0], command);
		new_entry->values = calloc(4, sizeof(int));
		add_values(command, new_entry);


	} else {
		// Head of list not null.
		// Iterate through list searching for entry with given key.
		//	If none exists, create one.
		entry *current = entries;

		while (1) {
			if (!strcmp(current->key, command)) {
				free(current->values);
				current->total_length = 4;
				current->value_length = 0;
				current->values = calloc(current->total_length, sizeof(int));
				add_values(command, current);
				return; // Skip adding a new node, return to main for input.
			} else {
				if (current->next == NULL) {
					break;
				}
				current = current->next;
			}
		}

		// No node found matching key. Creating and adding new node.
		entry *new_entry = calloc(1, sizeof(entry));
		new_entry->total_length = 4;
		new_entry->value_length = 0;
		current = entries;
		entries = new_entry;
		new_entry->next = current;
		current->prev = new_entry;

		// Key.
		strcpy(&new_entry->key[0], command);
		new_entry->values = calloc(4, sizeof(int));
		add_values(command, new_entry);

	}

}

void push(char *command) {

	// Checking case of no entries.
	if (entries == NULL) {
		printf("no such key\n");
		return;
	}

	// Iterate through entries, to find node
	entry *current = entries;
	while (1) {
		if (!strcmp(command, current->key)) {
			// Similar algorithm to add_values(), but add to front of array.
			command = strtok(NULL, "\n "); // A value.
			while (command != NULL) {
				// If array is full, resize. Else, add value to front of
				//	array of values.
				if ((current->total_length) - 1 == current->value_length) {
					current->values = realloc(current->values,
									  (current->total_length * 2)*sizeof(int));
					current->total_length = (current->total_length * 2);
				} else {
					for (int i = current->value_length - 1; i >= 0; i--) {
						current->values[i + 1] = current->values[i];
					}
					current->values[0] = atoi(command);
					current->value_length++;
					command = strtok(NULL, "\n "); // A value.
				}
			}
			printf("ok\n");
			break;
		} else {
			if (current->next == NULL) {
				printf("no such key\n");
				break;
			}
			current = current->next;
		}
	}
}

void append(char *command) {

	// Checking case of no entries.
	if (entries == NULL) {
		printf("no such key\n");
		return;
	}

	// Iterate through entries, to find node.
	entry *current = entries;
	while (1) {
		if (!strcmp(command, current->key)) {
			add_values(command, current);
			break;
		} else {
			if (current->next == NULL) {
				printf("no such key\n");
				break;
			}
			current = current->next;
		}
	}
}

// Stack value functions.
void pick(char *command) {

	// Checking case of no entries.
	if (entries == NULL) {
		printf("no such key\n");
		return;
	}

	// Iterate through entries
	entry *current = entries;
	while (1) {
		if (!strcmp(command, current->key)) {
			command = strtok(NULL, "\n "); // A value.
			// Check value in correct bounds.
			if ((atoi(command)) <= 0 || (atoi(command)) > current->value_length) {
				printf("index out of range\n");
				break;
			}
			printf("%d\n", current->values[atoi(command) - 1]);
			break;
		} else {
			if (current->next == NULL) {
				printf("no such key\n");
				break;
			}
			current = current->next;
		}
	}
}

int pluck(char *command) {
	// Similar to pick, but remove value at index. shift others down.

	// Checking case of no entries.
	if (entries == NULL) {
		printf("no such key\n");
		return INT_MIN;
	}

	// Iterate through entries
	entry *current = entries;
	while (1) {
		if (!strcmp(command, current->key)) {

			command = strtok(NULL, "\n "); // A value.

			// Check value in correct bounds.
			if ((atoi(command)) <= 0 || (atoi(command)) > current->value_length) {
				printf("index out of range\n");
				break;
			}
			int return_val = current->values[atoi(command) - 1];
			//printf("%d\n", current->values[atoi(command) - 1]);

			// Now, remove the value and shift others down.
			for (int i = (atoi(command)); i < current->value_length; i++) {
				current->values[i-1] = current->values[i];
			}
			// Then set the last value to 0, and decrease # of elements.
			current->values[current->value_length-1] = 0;
			current->value_length--;
			return return_val;

		} else {
			if (current->next == NULL) {
				printf("no such key\n");
				break;
			}
			current = current->next;
		}
	}
	return INT_MIN;
}

void pop(char *command) {
	entry *current = entries;

	// Checking case of no entries.
	if (entries == NULL) {
		printf("no such key\n");
		return;
	}

	while (1) {
		if (!strcmp(command, current->key)) {

			// Check entry contains values
			if (current->value_length <= 0) {
				printf("nil\n");
				break;
			}
			printf("%d\n", current->values[0]);

			// Now, remove the value and shift others down.
			for (int i = 1; i < current->value_length; i++) {
				current->values[i-1] = current->values[i];
			}
			// Then set the last value to 0, and decrease # of elements.
			current->values[current->value_length-1] = 0;
			current->value_length--;

			break;
		} else {
			if (current->next == NULL) {
				printf("no such key\n");
				break;
			}
			current = current->next;
		}
	}
}

// Snapshot functions.
void drop(char *command) {
	// Iterate through entries, to find node.
	snapshot *current = snapshots;
	while (1) {
		if (current == NULL || entries == NULL) {
			printf("no such snapshot\n");
			break;
		}

		if (current->id == atoi(command)) {

			// Freeing entries list.
			entry *next = current->entries;
			entry *old = next->prev;
			while (next != NULL) {
				old = next;
				next = next->next;
				free(old->values);
			}
			free(current->entries);

			// Freeing snapshot struct.
			if (current->prev != NULL) {
				current->prev->next = current->next;
			} else {
				snapshots = current->next;
			}
			if (current->next != NULL) {
				current->next->prev = current->prev;
			}
			free(current);
			printf("ok\n");
			break;
		} else {
			if (current == NULL || current->next == NULL) {
				printf("no such snapshot\n");
				break;
			}
			current = current->next;
		}
	}
}

void rollback(char *command) {
	// Iterate through snapshots to find required snapshot.
	snapshot *cur_snap = snapshots;
	while (1) {
		if (cur_snap == NULL) {
			printf("no such snapshot\n");
			return;
		}

		if (cur_snap->id == atoi(command)) {
			// Desired snapshot found. exit loop.
			break;
		} else {
			cur_snap = cur_snap->next;
		}
	}

	// Remove all entries in current state.
	while (entries != NULL) {
		del(entries->key);
	}

	// Fill current state with the entries from the snapshot.
	entry * snap_entry = cur_snap->entries;
	while (snap_entry != NULL) {
		entry *new_entry = calloc(1, sizeof(entry));
		sprintf(new_entry->key, "%s", snap_entry->key);
		new_entry->value_length = snap_entry->value_length;
		new_entry->total_length = snap_entry->total_length;
		new_entry->values = calloc(new_entry->total_length, sizeof(int));
		for (int i = 0; i < snap_entry->value_length; i++) {
			new_entry->values[i] = snap_entry->values[i];
		}
		snap_entry = snap_entry->next;

		//Iterate to end of current state list, add new entry.
		entry * current_state_entry = entries;

		//Special case when adding to an empty list
		if (entries == NULL) {
			entries = new_entry;
			continue;
		}
		while (current_state_entry != NULL) {
			if (current_state_entry->next == NULL) {
				current_state_entry->next = new_entry;
				new_entry->prev = current_state_entry;
				break;
			} else {
				current_state_entry = current_state_entry->next;
			}
		}

	}

	// Remove all snapshots after the chosen snapshot.
	cur_snap = cur_snap->next;
	while (cur_snap != NULL) {
		// If next snapshot is null, reached end of snapshots. Free and return.
		// Else, free and iterate to next snapshot.
		snap_entry = cur_snap->entries;
		if (cur_snap->next == NULL) {
			while (snap_entry != NULL) {
				// If next entry is NULL, reached end of entries. Free and return.
				//	Else, free and iterate to next entry.
				if (snap_entry->next == NULL) {
					free(snap_entry->values);
					free(snap_entry);
					break;
				} else {
					snap_entry = snap_entry->next;
					free(snap_entry->prev->values);
					free(snap_entry->prev);
				}
			}
			if (cur_snap->prev != NULL) {
				cur_snap->prev->next = NULL;
			}
			free(cur_snap);
			printf("ok\n");
			return;
		} else {
			while (snap_entry != NULL) {
				// If next entry is NULL, reached end of entries. Free and return.
				//	Else, free and iterate to next entry.
				if (snap_entry->next == NULL) {
					free(snap_entry->values);
					free(snap_entry);
					break;
				} else {
					snap_entry = snap_entry->next;
					free(snap_entry->prev->values);
					free(snap_entry->prev);
				}
			}
			cur_snap = cur_snap->next;
			free(cur_snap->prev);
		}
	}

}

void checkout(char *command) {
	// Iterate through snapshots to find required snapshot.
	snapshot *cur_snap = snapshots;
	while (1) {
		if (cur_snap == NULL) {
			printf("no such snapshot\n");
			return;
		}

		if (cur_snap->id == atoi(command)) {
			// Desired snapshot found. exit loop.
			break;
		} else {
			cur_snap = cur_snap->next;
		}
	}

	// Remove all entries in current state.
	while (entries != NULL) {
		del(entries->key);
	}

	// Fill current state with the entries from the snapshot.
	entry * snap_entry = cur_snap->entries;
	while (snap_entry != NULL) {
		entry *new_entry = calloc(1, sizeof(entry));
		sprintf(new_entry->key, "%s", snap_entry->key);
		new_entry->value_length = snap_entry->value_length;
		new_entry->total_length = snap_entry->total_length;
		new_entry->values = calloc(new_entry->total_length, sizeof(int));
		for (int i = 0; i < snap_entry->value_length; i++) {
			new_entry->values[i] = snap_entry->values[i];
		}
		snap_entry = snap_entry->next;

		//Iterate to end of current state list, add new entry.
		entry * current_state_entry = entries;

		//Special case when adding to an empty list
		if (entries == NULL) {
			entries = new_entry;
			continue;
		}
		while (current_state_entry != NULL) {
			if (current_state_entry->next == NULL) {
				current_state_entry->next = new_entry;
				new_entry->prev = current_state_entry;
				break;
			} else {
				current_state_entry = current_state_entry->next;
			}
		}

	}
	printf("ok\n");
}

void save_snapshot() {

	// Creating new snapshot to store current state in.
	snapshot *new_snap = calloc(1, sizeof(snapshot));

	// Iterating through entries in current state, adding to list of entries in
	//	the new snapshot.
	entry *current = entries;
	while (1) {
		// If current entry is NULL, the end of list has been reached.
		if (current == NULL) {
			break;
		}

		// Creating new entry to store the current entry in the snapshot.
		entry *new_entry = calloc(1, sizeof(entry));
		sprintf(new_entry->key, "%s", current->key);
		new_entry->value_length = current->value_length;
		new_entry->total_length = current->total_length;
		new_entry->values = calloc(new_entry->value_length, sizeof(int));
		for (int i = 0; i < new_entry->value_length; i++) {
			new_entry->values[i] = current->values[i];
		}

		// Iterating through entries in snapshot to add new entry.
		entry *snap_entry = new_snap->entries;
		while (1) {
			// If new_snap->entries is NULL, then special case where we are
			//	adding the first entry of the snapshot.
			if (new_snap->entries == NULL) {
				new_snap->entries = new_entry;
				break;
			}

			// If snap_entry->next == NULL, reached end of list. Add new
			//	entry here and break.
			if (snap_entry->next == NULL) {
				snap_entry->next = new_entry;
				new_entry->prev = snap_entry;
				break;
			} else { // Else continue iterating through snapshot entries.
				snap_entry = snap_entry->next;
			}

		}

		if (current->next == NULL) {
			break;
		} else {
			current = current->next;
		}

	}

	// Iterating through list of snapshots to add new snapshot.
	snapshot *current_snap = snapshots;
	while (1) {
		// If snapshots == NULL, special case where we are adding the first
		//	snapshot of the list.
		if (snapshots == NULL) {
			snapshots = new_snap;
			new_snap->id = id;
			break;
		}

		// If current_snap->next == NULL, reached end of list. Add new snapshot here
		//	and break.
		if (current_snap->next == NULL) {
			current_snap->next = new_snap;
			new_snap->prev = current_snap;
			new_snap->id = id;
			break;
		} else {
			current_snap = current_snap->next;
		}

	}

	printf("saved as snapshot %d\n", id);
	id++;

}

// Aggregate functions.
void min(char *command) {
	// Iterate through entries, to find node.
	entry *current = entries;
	if (entries == NULL) {
		printf("no such key\n");
		return;
	}
	while (1) {
		if (!strcmp(command, current->key)) {
			int min = INT_MAX;
			for (int i = 0; i < current->value_length; i++) {
				if (current->values[i] < min) {
					min = current->values[i];
				}
			}
			printf("%d\n", min);
			break;
		} else {
			if (current->next == NULL) {
				printf("no such key\n");
				break;
			}
			current = current->next;
		}
	}
}

void max(char *command) {
	// Iterate through entries, to find node.
	entry *current = entries;
	if (entries == NULL) {
		printf("no such key\n");
		return;
	}
	while (1) {
		if (!strcmp(command, current->key)) {
			int max = INT_MIN;
			for (int i = 0; i < current->value_length; i++) {
				if (current->values[i] > max) {
					max = current->values[i];
				}
			}
			printf("%d\n", max);
			break;
		} else {
			if (current->next == NULL) {
				printf("no such key\n");
				break;
			}
			current = current->next;
		}
	}
}

void sum(char *command) {
	// Iterate through entries, to find node.
	entry *current = entries;
	if (entries == NULL) {
		printf("no such key\n");
		return;
	}
	while (1) {
		if (!strcmp(command, current->key)) {
			int count = 0;
			for (int i = 0; i < current->value_length; i++) {
				count += current->values[i];
			}
			printf("%d\n", count);
			break;
		} else {
			if (current->next == NULL) {
				printf("no such key\n");
				break;
			}
			current = current->next;
		}
	}
}

void len(char *command) {
	// Iterate through entries, to find node.
	entry *current = entries;
	if (entries == NULL) {
		printf("no such key\n");
		return;
	}
	while (1) {
		if (!strcmp(command, current->key)) {
			printf("%zu\n", current->value_length);
			break;
		} else {
			if (current->next == NULL) {
				printf("no such key\n");
				break;
			}
			current = current->next;
		}
	}
}

// Sorting functions.
void rev(char *command) {

	// Checking case of no entries.
	if (entries == NULL) {
		printf("no such key\n");
		return;
	}

	// Iterate through entries, to find node.
	entry *current = entries;
	while (1) {
		if (!strcmp(command, current->key)) {
			for (int i = 0; i <= ((current->value_length - 1) / 2); i++) {
				int temp = current->values[i];
				current->values[i] = current->values[current->value_length - 1 - i];
				current->values[current->value_length - 1 - i] = temp;
			}
			printf("ok\n");
			break;
		} else {
			if (current->next == NULL) {
				printf("no such key\n");
				break;
			}
			current = current->next;
		}
	}
}

int uniq(char *command) {
	// Iterate through entries, to find node.
	entry *current = entries;
	if (entries == NULL) {
		printf("no such key\n");
		return INT_MIN;
	}
	while (1) {
		if (!strcmp(command, current->key)) {
			for (int i = 1; i <= current->value_length - 1; i++) {
				if (current->values[i] == current->values[i - 1] ||
					current->values[i] == current->values[i + 1]) {
					// Delete the current value, resize array, restart for loop.
					for (int j = i; j <= current->value_length - 1; j++) {
						current->values[j] = current->values[j + 1];
					}
					current->value_length--;
					i = 0;
				}
			}
			return 0;
		} else {
			if (current->next == NULL) {
				printf("no such key\n");
				return INT_MIN;
			}
			current = current->next;
		}
	}
}

// Function adapted from
//	http://www.tutorialspoint.com/c_standard_library/c_function_qsort.htm
int compare (const void * a, const void * b) {
   return ( *(int*)a - *(int*)b );
}

int sort(char *command) {
	// Iterate through entries, to find node.
	entry *current = entries;
	if (entries == NULL) {
		printf("no such key\n");
		return INT_MIN;
	}
	while (1) {
		if (!strcmp(command, current->key)) {
			qsort(current->values, current->value_length, sizeof(int), compare);
			return 0;
		} else {
			if (current->next == NULL) {
				printf("no such key\n");
				return INT_MIN;
			}
			current = current->next;
		}
	}
}

// Set functions.

// Helper Functions.

int* set_sort(int *array, int array_num_values) {
	qsort(array, array_num_values, sizeof(int), compare);
	return array;
}
int set_uniq(int *array, int array_num_values) {
	for (int i = 1; i <= array_num_values - 1; i++) {
		if (array[i] == array[i - 1] ||
			array[i] == array[i + 1]) {
			// Delete the current value, resize array, restart for loop.
			for (int j = i; j <= array_num_values - 1; j++) {
				array[j] = array[j + 1];
			}
			array_num_values--;
			i = 0;
		}
	}
	return array_num_values;
}
int contains(int num, int *array, int array_num_values) {
	for (int i = 0; i < array_num_values; i++) {
		if (array[i] == num) {
			return 1;
		}
	}
	return 0;
}

void set_print(int *array, int array_num_values) {
	printf("[");
	if (array_num_values >= 1) {
		printf("%d", array[0]);
	}
	for (int i = 1; i < array_num_values; i++) {
		printf(" %d", array[i]);
	}
	printf("]\n");
}

int *calc_inter(char* command, int *num) {
	int *array_a = calloc(4, sizeof(int));
	int array_a_length = 4;
	int array_a_num_values = 0;
	int *array_b = calloc(4, sizeof(int));
	int array_b_length = 4;
	int array_b_num_values = 0;
	int *array_r = calloc(4, sizeof(int));
	int array_r_length = 4;
	int array_r_num_values = 0;
	// Fill array_a & array_b with values from specified entries.
	entry *current = entries;
	// Array_a.
	while (1) {
		if (!strcmp(command, current->key)) {
			for (int i = 0; i < current->value_length; i++) {
				// If array is full, resize. Else, add value to array of values.
				if ((array_a_length)-1 == array_a_num_values) {
					array_a = realloc(array_a, (array_a_length * 2)*sizeof(int));
					array_a_length = (array_a_length * 2);
					i--;
				} else {
					array_a[array_a_num_values] = current->values[i];
					array_a_num_values++;
				}
			}
			break;
		} else {
			if (current->next == NULL) {
				printf("no such key\n");
				return NULL;
			}
			current = current->next;
		}
	}

	command = strtok(NULL, "\n ");
	// Loop on computing the union set of array_a and array_b.
	while (1) {
		// Array_b.
		current = entries;
		while (1) {
			if (!strcmp(command, current->key)) {
				for (int i = 0; i < current->value_length; i++) {
					// If array is full, resize. Else, add value to array of values.
					if ((array_b_length)-1 == array_b_num_values) {
						array_b = realloc(array_b, (array_b_length * 2)*sizeof(int));
						array_b_length = (array_b_length * 2);
						i--;
					} else {
						array_b[array_b_num_values] = current->values[i];
						array_b_num_values++;
					}
				}
				break;
			} else {
				if (current->next == NULL) {
					printf("no such key\n");
					return NULL;
				}
				current = current->next;
			}
		}
		array_a = set_sort(array_a, array_a_num_values);
		array_a_num_values = set_uniq(array_a, array_a_num_values);

		array_b = set_sort(array_b, array_b_num_values);
		array_b_num_values = set_uniq(array_b, array_b_num_values);

		for (int i = 0; i < array_a_num_values; i++) {
			if (contains(array_a[i], array_b, array_b_num_values)) {
				// If array is full, resize. Else, add value to array of values.
				if ((array_r_length)-1 == array_r_num_values) {
					array_r = realloc(array_r,
										  (array_r_length * 2)*sizeof(int));
					array_r_length = (array_r_length * 2);
					i--;
				} else {
					array_r[array_r_num_values] = array_a[i];
					array_r_num_values++;
				}
			}
		}
		// If no more input, return and end. Free all arrays.
		// Else, array_a = array_r, array_b = new array from imput,
		//	array_r = new array.
		command = strtok(NULL, "\n ");
		if (command == NULL) {
			free(array_a);
			free(array_b);
			*num = array_r_num_values;
			return array_r;
		} else {
			free(array_a);
			array_a = array_r;
			array_a_length = array_r_length;
			array_a_num_values = array_r_num_values;

			free(array_b);
			array_b = calloc(4, sizeof(int));
			array_b_length = 4;
			array_b_num_values = 0;

			array_r = calloc(4, sizeof(int));
			array_r_length = 4;
			array_r_num_values = 0;

			continue;
		}
	}
	return NULL;
}
int *calc_union(char* command, int *num) {
	int *array_a = calloc(4, sizeof(int));
	int array_a_length = 4;
	int array_a_num_values = 0;
	int *array_b = calloc(4, sizeof(int));
	int array_b_length = 4;
	int array_b_num_values = 0;
	int *array_r = calloc(4, sizeof(int));
	int array_r_length = 4;
	int array_r_num_values = 0;
	// Fill array_a & array_b with values from specified entries.
	entry *current = entries;
	// Array_a.
	while (1) {
		if (!strcmp(command, current->key)) {
			for (int i = 0; i < current->value_length; i++) {
				// If array is full, resize. Else, add value to array of values.
				if ((array_a_length)-1 == array_a_num_values) {
					array_a = realloc(array_a, (array_a_length * 2)*sizeof(int));
					array_a_length = (array_a_length * 2);
					i--;
				} else {
					array_a[array_a_num_values] = current->values[i];
					array_a_num_values++;
				}
			}
			break;
		} else {
			if (current->next == NULL) {
				printf("no such key\n");
				return NULL;
			}
			current = current->next;
		}
	}

	command = strtok(NULL, "\n ");
	// Loop on computing the union set of array_a and array_b.
	while (1) {
		// Array_b.
		current = entries;
		while (1) {
			if (!strcmp(command, current->key)) {
				for (int i = 0; i < current->value_length; i++) {
					// If array is full, resize. Else, add value to array of values.
					if ((array_b_length)-1 == array_b_num_values) {
						array_b = realloc(array_b, (array_b_length * 2)*sizeof(int));
						array_b_length = (array_b_length * 2);
						i--;
					} else {
						array_b[array_b_num_values] = current->values[i];
						array_b_num_values++;
					}
				}
				break;
			} else {
				if (current->next == NULL) {
					printf("no such key\n");
					return NULL;
				}
				current = current->next;
			}
		}

		//Adding all values from array_a and array_b.
		for (int i = 0; i < array_a_num_values; i++) {

			// If array is full, resize. Else, add value to array of values.
			if ((array_r_length)-1 == array_r_num_values) {
				array_r = realloc(array_r,
									  (array_r_length * 2)*sizeof(int));
				array_r_length = (array_r_length * 2);
				i--;
			} else {
				array_r[array_r_num_values] = array_a[i];
				array_r_num_values++;
			}

		}
		for (int i = 0; i < array_b_num_values; i++) {

			// If array is full, resize. Else, add value to array of values.
			if ((array_r_length)-1 == array_r_num_values) {
				array_r = realloc(array_r,
									  (array_r_length * 2)*sizeof(int));
				array_r_length = (array_r_length * 2);
				i--;
			} else {
				array_r[array_r_num_values] = array_b[i];
				array_r_num_values++;
			}

		}


		// Sort and uniq array.
		array_r = set_sort(array_r, array_r_num_values);
		array_r_num_values = set_uniq(array_r, array_r_num_values);

		// If no more input, return and end. Free all arrays.
		// Else, array_a = array_r, array_b = new array from imput,
		//	array_r = new array.
		command = strtok(NULL, "\n ");
		if (command == NULL) {
			free(array_a);
			free(array_b);
			*num = array_r_num_values;
			return array_r;
		} else {
			free(array_a);
			array_a = array_r;
			array_a_length = array_r_length;
			array_a_num_values = array_r_num_values;

			free(array_b);
			array_b = calloc(4, sizeof(int));
			array_b_length = 4;
			array_b_num_values = 0;

			array_r = calloc(4, sizeof(int));
			array_r_length = 4;
			array_r_num_values = 0;

			continue;
		}
	}
	return NULL;
}
int *calc_diff(char *command, int *num) {
	int *array_a = calloc(4, sizeof(int));
	int array_a_length = 4;
	int array_a_num_values = 0;

	int *array_b = calloc(4, sizeof(int));
	int array_b_length = 4;
	int array_b_num_values = 0;

	int *array_r = calloc(4, sizeof(int));
	int array_r_length = 4;
	int array_r_num_values = 0;

	entry *current = entries;

	// Fill array_a.
	while (1) {
		if (!strcmp(command, current->key)) {
			for (int i = 0; i < current->value_length; i++) {
				// If array is full, resize. Else, add value to array of values.
				if ((array_a_length)-1 == array_a_num_values) {
					array_a = realloc(array_a, (array_a_length * 2)*sizeof(int));
					array_a_length = (array_a_length * 2);
					i--;
				} else {
					array_a[array_a_num_values] = current->values[i];
					array_a_num_values++;
				}
			}
			break;
		} else {
			if (current->next == NULL) {
				printf("no such key\n");
				return NULL;
			}
			current = current->next;
		}
	}

	command = strtok(NULL, "\n ");
	// Loop on computing the diff set of array_a and array_b.
	while (1) {
		// Fill array_b.
		current = entries;
		while (1) {
			if (!strcmp(command, current->key)) {
				for (int i = 0; i < current->value_length; i++) {
					// If array is full, resize. Else, add value to array of values.
					if ((array_b_length)-1 == array_b_num_values) {
						array_b = realloc(array_b, (array_b_length * 2)*sizeof(int));
						array_b_length = (array_b_length * 2);
						i--;
					} else {
						array_b[array_b_num_values] = current->values[i];
						array_b_num_values++;
					}
				}
				break;
			} else {
				if (current->next == NULL) {
					printf("no such key\n");
					return NULL;
				}
				current = current->next;
			}
		}

		// Compute array_r:
		// Adding elements from array_a:
		for (int i = 0; i < array_a_num_values; i++) {
			if (((contains(array_a[i], array_a, array_a_num_values))  ||
				 (contains(array_a[i], array_b, array_b_num_values))) &&
			   !((contains(array_a[i], array_a, array_a_num_values))  &&
				 (contains(array_a[i], array_b, array_b_num_values)))) {
				// If array is full, resize. Else, add value to array of values.
				if ((array_r_length)-1 == array_r_num_values) {
					array_r = realloc(array_r,
									 (array_r_length * 2)*sizeof(int));
					array_r_length = (array_r_length * 2);
					i--;
				} else {
					array_r[array_r_num_values] = array_a[i];
					array_r_num_values++;
				}
			}
		}
		// Adding elements from array_b:
		for (int i = 0; i < array_b_num_values; i++) {
			if (((contains(array_b[i], array_a, array_a_num_values))  ||
				 (contains(array_b[i], array_b, array_b_num_values))) &&
			   !((contains(array_b[i], array_a, array_a_num_values))  &&
				 (contains(array_b[i], array_b, array_b_num_values)))) {
				// If array is full, resize. Else, add value to array of values.
				if ((array_r_length)-1 == array_r_num_values) {
					array_r = realloc(array_r,
									 (array_r_length * 2)*sizeof(int));
					array_r_length = (array_r_length * 2);
					i--;
				} else {
					array_r[array_r_num_values] = array_b[i];
					array_r_num_values++;
				}
			}
		}
		// Sort and uniq array.
		array_r = set_sort(array_r, array_r_num_values);
		array_r_num_values = set_uniq(array_r, array_r_num_values);

		// If no more input, return and end. Free all arrays.
		// Else, array_a = array_r, array_b = new array from imput,
		//	array_r = new array.
		command = strtok(NULL, "\n ");
		if (command == NULL) {
			free(array_a);
			free(array_b);
			*num = array_r_num_values;
			return array_r;
		} else {
			free(array_a);
			array_a = array_r;
			array_a_length = array_r_length;
			array_a_num_values = array_r_num_values;

			free(array_b);
			array_b = calloc(4, sizeof(int));
			array_b_length = 4;
			array_b_num_values = 0;

			array_r = calloc(4, sizeof(int));
			array_r_length = 4;
			array_r_num_values = 0;

			continue;
		}
	}


}

void diff(char *command) {
	int num;
	int *array_ret = calc_diff(command, &num);
	set_print(array_ret, num);
	free(array_ret);
}

void inter(char *command) {
	int num;
	int *array_ret = calc_inter(command, &num);
	set_print(array_ret, num);
	free(array_ret);
}

void set_union(char *command) {
	int num;
	int *array_ret = calc_union(command, &num);
	set_print(array_ret, num);
	free(array_ret);
}

void command_bye() {

	// Iterate through entries, free memory.
	while (1) {
		entry *current = entries;
		if (current == NULL) {
			break;
		}
		del(current->key);
	}
	free(entries);

	// Iterate through snapshots, free memory.
	snapshot *cur_snap = snapshots;
	while (1) {

		// If snapshots == NULL, special case where there are no snapshots.
		if (snapshots == NULL) {
			break;
		}

		// Iterate through entries in snapshot, free memory.
		entry *snap_entry = cur_snap->entries;
		while (1) {

			// If cur_snap->entries == NULL, special case where there are no
			//	entries.
			if (cur_snap->entries == NULL) {
				break;
			}

			// Free values from entry.
			free(snap_entry->values);

			// If snap_entry->next == NULL, reached end of list.
			if (snap_entry->next == NULL) {
				free(snap_entry);
				break;
			} else {
				snap_entry = snap_entry->next;
				free(snap_entry->prev);
				cur_snap->entries = snap_entry;
			}

		}

		snapshot *next_snap = cur_snap->next;

		// If next_snap == NULL, cur_snap is the last snapshot.
		// Else, there are more snaps to iterate through.
		if (next_snap == NULL) {
			free(cur_snap);
			break;
		} else {
			cur_snap = next_snap;
			free(cur_snap->prev);
		}

	}

	printf("bye\n");
	exit(0);
}

void command_help() {
	printf("%s\n", HELP);
}

// Simple hash function which produces a unique number for each command.
int hash(char *str) {
		int hash = 0;
		while (*str){
			hash = (hash + *str) * *str - *str;
			str++;
		}
		hash = hash % 1000;
		return hash;
}

int main(void) {

	char line[MAX_LINE];

	while (true) {
		printf("> ");

		if (fgets(line, MAX_LINE, stdin) == NULL) {
			printf("\n");
			command_bye();
			return 0;
		}

		// Isolating the command from the users' input.
		char *command = strtok(line, "\n ");

		// If line enered was null, command will be null.
		if (command == NULL) {
			continue;
		}

		// Command is case insensitive, so converting command to lowercase.
		for (int i = 0; i < strlen(command); i++) {
			command[i] = tolower(command[i]);
		}

		// Hashing command for use in switch statement.
		int hash_val = hash(command);

		//moving pointer for command to next argument in user input line
		//	(null if line is a single argument).
		command = strtok(NULL, "\n ");
		int result = INT_MIN; // Used in various functions.

		// Jump to function based on value of hash.
		switch (hash_val) {
			case -84: /*list*/
				list(command);
				printf("\n");
				break;
			case 236: /*get*/
				get(command);
				printf("\n");
				break;
			case 556: /*del*/
				result = del(command);
				if (result != INT_MIN) {
					printf("ok\n");
				}
				printf("\n");
				break;
			case 44: /*purge*/
				purge(command);
				printf("\n");
				break;
			case 700: /*set*/
				set(command);
				printf("\n");
				break;
			case 328: /*push*/
				push(command);
				printf("\n");
				break;
			case 516: /*append*/
				append(command);
				printf("\n");
				break;
			case 608: /*pick*/
				pick(command);
				printf("\n");
				break;
			case -312: /*pluck*/
				result = pluck(command);
				if (result != INT_MIN) {
					printf("%d\n", result);
				}
				printf("\n");
				break;
			case 576: /*pop*/
				pop(command);
				printf("\n");
				break;
			case 288: /*drop*/
				drop(command);
				printf("\n");
				break;
			case -414: /*rollback*/
				rollback(command);
				printf("\n");
				break;
			case 684: /*checkout*/
				checkout(command);
				printf("\n");
				break;
			case -588: /*snapshot*/
				save_snapshot();
				printf("\n");
				break;
			case 790: /*min*/
				min(command);
				printf("\n");
				break;
			case 800: /*max*/
				max(command);
				printf("\n");
				break;
			case 950: /*sum*/
				sum(command);
				printf("\n");
				break;
			case 150: /*len*/
				len(command);
				printf("\n");
				break;
			case 282: /*rev*/
				rev(command);
				printf("\n");
				break;
			case 582: /*uniq*/
				result = uniq(command);
				if (result != INT_MIN) {
					printf("ok\n");
				}
				printf("\n");
				break;
			case -748: /*sort*/
				result = sort(command);
				if (result != INT_MIN) {
					printf("ok\n");
				}
				printf("\n");
				break;
			case -102: /*diff*/
				diff(command);
				printf("\n");
				break;
			case 10: /*inter*/
				inter(command);
				printf("\n");
				break;
			case 886: /*union*/
				set_union(command);
				printf("\n");
				break;
			case 968: /*help*/
				command_help();
				break;
			case 446: /*bye*/
				command_bye();
				break;
		}

  	}

	return 0;
}
